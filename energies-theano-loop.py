#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import theano
from theano import tensor as TT

n_spins = 108
J = np.load('interaction.npy')
rnd = np.random.RandomState()

t_s = TT.bvector('s')
t_J = TT.bmatrix('J')


def main_loop(i, t_s, t_J):
    def inner_loop(j, t_s, t_J, i, dE):
        return dE - 2 * t_s[j] * theano.dot(t_J[j], t_s)
    dE = -2 * t_s[i] * theano.dot(t_J[i], t_s)
    dE2, _ = theano.scan(fn=inner_loop, sequences=TT.arange(108),
        non_sequences=[TT.set_subtensor(t_s[i], -t_s[i]), t_J, i, dE])
    return dE2

result, _ = theano.scan(fn=main_loop, sequences=TT.arange(108),
                        non_sequences=[t_s, t_J])

delta_E_theano = theano.function([t_s, t_J], result)


def test_energy_calc():
    for step in range(1000):
        spins = rnd.choice(np.array([-1, 1], dtype='int8'), n_spins)

        dEs = delta_E_theano(spins, J)

test_energy_calc()
