﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import theano
from theano import tensor as TT


n_spins = 108
J = np.load('interaction.npy')
rnd = np.random.RandomState()
dEs = np.ones(n_spins * (n_spins + 1) / 2, dtype='int8')


t_s = TT.bvector('s')
t_J = TT.bmatrix('J')
t_i = TT.bvector('i')


def t_dE(t_i):
    return -2 * t_s[t_i] * theano.dot(t_J[t_i], t_s)

delta_E_theano = theano.function([t_i, t_s, t_J], t_dE(t_i))


def test_energy_calc():
    for step in range(1000):
        pos = 0
        spins = rnd.choice(np.array([-1, 1], dtype='int8'), n_spins)
        for i in range(n_spins):
            # single spin flip energie
            dE = -2 * spins[i] * np.dot(J[i], spins)
            dEs[pos] = dE
            pos += 1
            spins[i] *= -1  # inplace flip, must be undone
            # double flip energie
            dE2 = dE + delta_E_theano(
                np.arange(i + 1, n_spins, dtype='int8'), spins, J)
            dEs[pos:pos + len(dE2)] = dE2
            pos += len(dE2)
            spins[i] *= -1  # undoing inplace flip

test_energy_calc()
