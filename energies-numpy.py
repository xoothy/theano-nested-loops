#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

n_spins = 108
J = np.load('interaction.npy')
rnd = np.random.RandomState()
dEs = np.ones(n_spins * (n_spins + 1) / 2, dtype='int8')


def test_energy_calc():
    for step in range(1000):
        pos = 0
        spins = rnd.choice(np.array([-1, 1], dtype='int8'), n_spins)
        for i in range(n_spins):
            # single spin flip energie
            dE = -2 * spins[i] * np.dot(J[i], spins)
            dEs[pos] = dE
            pos += 1
            spins[i] *= -1  # inplace flip, must be undone
            for j in range(i + 1, n_spins):
                # double flip energie
                dE2 = dE - 2 * spins[j] * np.dot(J[j], spins)
                dEs[pos] = dE2
                pos += 1
            spins[i] *= -1  # undoing inplace flip

test_energy_calc()
